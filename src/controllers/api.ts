import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'

import * as crypto from 'crypto';

import { UserModel } from '../models/user';

export default async function index(fastify: FastifyInstance) {

  const userModel = new UserModel();
  const db = fastify.db;

  // Users
  fastify.get('/users', {
    preValidation: [fastify.authenticate]
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const res: any = await userModel.list(db)
    reply.send({ results: res })
  })

  fastify.delete('/users/:id', {
    preValidation: [fastify.authenticate]
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const query: any = request.params;
    const id = query.id;

    await userModel.delete(id, db)
    reply.send({ ok: true })
  })

  fastify.post('/users', {
    preValidation: [fastify.authenticate]
  }, async (request: FastifyRequest, reply: FastifyReply) => {

    const params: any = request.body;

    params.password = crypto.createHash('md5')
      .update(params.password)
      .digest('hex');

    await userModel.create(params, db)
    reply.send({ ok: true })
  })

  fastify.put('/users/:id', {
    preValidation: [fastify.authenticate]
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const query: any = request.params;
    const params: any = request.body;

    delete params.cid;

    const id = query.id;

    await userModel.update(id, params, db)
    reply.send({ ok: true })
  })


  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.send({ message: "Hello, API Server" })
  })

  // DEMO
  // localhost:3000/demo/20/satit
  fastify.get('/demo/:id/:name', async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.params;
    const { id, name } = params;

    /*
    const id = params.id;
    const name = params.name;
    */

    reply.send({ id, name })
  })

  // localhost:3000/demo?id=xxx&name=yyyy
  fastify.get('/demo', async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.query;
    const { id, name } = params;

    /*
    const id = params.id;
    const name = params.name;
    */

    reply.send({ id, name })
  })

  fastify.post('/demo', async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.body;
    const { id, name } = params;

    reply.send({ id, name })
  })

  fastify.put('/demo/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.body;
    const { id, name } = params;

    reply.send({ id, name })
  })

  fastify.delete('/demo', async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.query;
    const { id, name } = params;
    reply.send({ id, name })
  })

}
import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { ServiceModel } from '../models/service'

export default async function index(fastify: FastifyInstance) {

  /*
  1. GET /search (cid)
  2. GET /service  (gw_hospcode, hn)
  3. GET /opdscreen (gw_hospcode , hn, vn)
  4. GET /drug (gw_hospcode, hn, vn)
  5. GET /diagnosis (gw_hospcode , hn, vn)
  6. GET /appointment (gw_hospcode , hn)
  7. GET /patient (gw_hospcode , hn)
  */

  const serviceModel = new ServiceModel();
  const db = fastify.db;

  // /search?cid=xxxxxxx
  fastify.get('/search', {
    preValidation: [fastify.authenticate]
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const query: any = request.query;
      const cid = query.cid;

      const res: any = await serviceModel.search(db, cid)
      reply.send({ ok: true, results: res[0] })
    } catch (error) {
      console.error(error);
      reply.send({ ok: false, error_code: 500, error_message: 'เกิดข้อผิดพลาดฝั่ง SERVER' })
    }
  })

  // /services?hospcode=xxx&hn=xxxx
  fastify.get('/service', {
    preValidation: [fastify.authenticate]
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const query: any = request.query;
      const hospcode = query.hospcode;
      const hn = query.hn;

      const res: any = await serviceModel.service(db, hospcode, hn)
      reply.send({ ok: true, results: res[0] })
    } catch (error) {
      console.error(error);
      reply.send({ ok: false, error_code: 500, error_message: 'เกิดข้อผิดพลาดฝั่ง SERVER' })
    }
  })

  fastify.get('/opdscreen', {
    preValidation: [fastify.authenticate]
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const query: any = request.query;
      const hospcode = query.hospcode;
      const hn = query.hn;
      const vn = query.vn;

      const res: any = await serviceModel.opdscreen(db, hospcode, hn, vn)
      reply.send({ ok: true, results: res[0] })
    } catch (error) {
      console.error(error);
      reply.send({ ok: false, error_code: 500, error_message: 'เกิดข้อผิดพลาดฝั่ง SERVER' })
    }
  })

  fastify.get('/drug', {
    preValidation: [fastify.authenticate]
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const query: any = request.query;
      const hospcode = query.hospcode;
      const hn = query.hn;
      const vn = query.vn;

      const res: any = await serviceModel.drug(db, hospcode, hn, vn)
      reply.send({ ok: true, results: res[0] })
    } catch (error) {
      console.error(error);
      reply.send({ ok: false, error_code: 500, error_message: 'เกิดข้อผิดพลาดฝั่ง SERVER' })
    }
  })


  fastify.get('/diagnosis', {
    preValidation: [fastify.authenticate]
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const query: any = request.query;
      const hospcode = query.hospcode;
      const hn = query.hn;
      const vn = query.vn;

      const res: any = await serviceModel.drug(db, hospcode, hn, vn)
      reply.send({ ok: true, results: res[0] })
    } catch (error) {
      console.error(error);
      reply.send({ ok: false, error_code: 500, error_message: 'เกิดข้อผิดพลาดฝั่ง SERVER' })
    }
  })

  fastify.get('/appointment', {
    preValidation: [fastify.authenticate]
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const query: any = request.query;
      const hospcode = query.hospcode;
      const hn = query.hn;

      const res: any = await serviceModel.appointment(db, hospcode, hn)
      reply.send({ ok: true, results: res[0] })
    } catch (error) {
      console.error(error);
      reply.send({ ok: false, error_code: 500, error_message: 'เกิดข้อผิดพลาดฝั่ง SERVER' })
    }
  })

  fastify.get('/patient', {
    preValidation: [fastify.authenticate]
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    try {
      const query: any = request.query;
      const hospcode = query.hospcode;
      const hn = query.hn;

      const res: any = await serviceModel.patient(db, hospcode, hn)
      reply.send({ ok: true, results: res[0] })
    } catch (error) {
      console.error(error);
      reply.send({ ok: false, error_code: 500, error_message: 'เกิดข้อผิดพลาดฝั่ง SERVER' })
    }
  })

}
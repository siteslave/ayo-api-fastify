import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import * as crypto from 'crypto';

import { LoginModel } from '../models/login'

export default async function index(fastify: FastifyInstance) {

  const loginModel = new LoginModel();
  const db = fastify.db;

  fastify.post('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const params: any = request.body;
    const { username, password } = params;
    const pincode = crypto.createHash('md5')
      .update(password)
      .digest('hex');

    const res: any = await loginModel.login(username, pincode, db);
    if (res.length > 0) {
      const userId: any = res[0].id;
      const token = fastify.jwt.sign({ id: userId }, { expiresIn: '1d' })
      reply.send({ ok: true, accessToken: token })
    } else {
      reply.send({ ok: false, error: 'ชื่อผู้ใช้งาน/รหัสผ่าน ไม่ถูกต้อง' });
    }

  })

}
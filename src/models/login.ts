import * as knex from 'knex';

export class LoginModel {

  login(cid: any, pincode: any, db: knex) {
    return db('person_pin')
      .select('id')
      .where('cid', cid)
      .where('pin', pincode)
      .limit(1);
  }

}
import { FastifyInstance } from 'fastify'

import indexRouter from './controllers/index'
import apiRouter from './controllers/api'
import loginRouter from './controllers/login'
import serviceRouter from './controllers/service'

export default async function router(fastify: FastifyInstance) {
  fastify.register(apiRouter, { prefix: '/api/v1' })
  fastify.register(loginRouter, { prefix: '/login' })
  fastify.register(serviceRouter, { prefix: '/services' })
  fastify.register(indexRouter, { prefix: '/' })
}